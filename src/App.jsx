/* eslint-disable*/
import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { store } from './store';

import Home from './containers/Home';
import Details from './containers/Details';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" store={store} component={Home} />
          <Route
            exact
            path="/details/:dependancies"
            store={store}
            component={Details}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
