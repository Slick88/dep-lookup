import React from 'react';
import { mount } from 'enzyme';
import { Table } from 'react-bootstrap';

import Details from './Details';

let DetailsWrapper;
let fetchDataMockFn;
const locationMockObj = {
  pathname: '/details/react',
};

const render = (props = {}) =>
  mount(
    <Details
      packageName=""
      list={[]}
      fetchData={fetchDataMockFn}
      location={locationMockObj}
      {...props}
    />,
  );

describe('Details - render with package not found', () => {
  beforeEach(() => {
    fetchDataMockFn = jest.fn();
    DetailsWrapper = render({
      packageName: '',
      list: [],
    });
  });
  it('should recieve props correctly', () => {
    expect(DetailsWrapper.props().packageName).toEqual('');
    expect(DetailsWrapper.props().list).toEqual([]);
    expect(DetailsWrapper.props().location).toEqual(locationMockObj);
  });

  it('should render header correctly', () => {
    expect(DetailsWrapper.find('h1').text()).toEqual('Package Overview');
  });
  it('should render package name correctly', () => {
    expect(DetailsWrapper.find('h3').text()).toEqual('Package is not found');
  });
  it('should render table correctly', () => {
    expect(DetailsWrapper.find(Table).exists()).toEqual(false);
  });
});

describe('Details - render with package found', () => {
  beforeEach(() => {
    fetchDataMockFn = jest.fn();
    DetailsWrapper = render({
      packageName: 'React',
      list: ['package1', 'package2'],
    });
  });
  it('should recieve props correctly', () => {
    expect(DetailsWrapper.props().packageName).toEqual('React');
    expect(DetailsWrapper.props().list).toEqual(['package1', 'package2']);
  });

  it('should render header correctly', () => {
    expect(DetailsWrapper.find('h1').text()).toEqual('Package Overview');
  });
  it('should render package name correctly', () => {
    expect(DetailsWrapper.find('h3').text()).toEqual('React');
  });
  it('should render table correctly', () => {
    expect(DetailsWrapper.find(Table).exists()).toEqual(true);
  });
});
