/* eslint-disable*/
import React, { Component } from 'react';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { Button, Container, Table } from 'react-bootstrap';

import Loader from '../../containers/Loader';
import styles from './Details.module.css';

class Details extends Component {
  constructor(props) {
    super(props);
    const dep = this.props.location.pathname.split('/')[2];
    console.log(dep);
    this.props.fetchData(dep);
  }

  renderRows = () => {
    const { list } = this.props;
    return list.map((dependency, index) => (
      <tr key={dependency}>
        <td>{index + 1}</td>
        <td>{dependency}</td>
      </tr>
    ));
  };

  renderTable = () => {
    return (
      <Table bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Modules</th>
          </tr>
        </thead>
        <tbody>{this.renderRows()}</tbody>
      </Table>
    );
  };

  renderPackageName = () => {
    const { packageName } = this.props;
    const packageisFound = !isEmpty(packageName);

    return (
      <h3 className={!packageisFound ? styles.packageNF : undefined}>
        {packageisFound ? packageName : 'Package is not found'}
      </h3>
    );
  };

  renderData = () => {
    const { list, packageName } = this.props;
    const hasDependancies = !isEmpty(list);
    const packageisFound = !isEmpty(packageName);
    return (
      <div>
        {this.renderPackageName()}
        {hasDependancies
          ? this.renderTable()
          : packageisFound && 'No Dependancies'}
      </div>
    );
  };

  onClickReturn = () => {
    this.props.history.push(`/`);
  };

  render() {
    const { isLoading } = this.props;

    return (
      <Container className={styles.pageOverview}>
        <h1 className="title">Package Overview</h1>
        {isLoading ? <Loader /> : this.renderData()}
        <Button variant="outline-primary" onClick={this.onClickReturn}>
          Return
        </Button>
      </Container>
    );
  }
}

Details.propTypes = {
  fetchData: PropTypes.func.isRequired,
  packageName: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default Details;
