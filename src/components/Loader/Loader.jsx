/*eslint-disable*/
import React, { Component } from 'react';
import { Spinner } from 'react-bootstrap';
import PropTypes from 'prop-types';

class Loader extends Component {
  render() {
    const { isLoading } = this.props;
    return <div>{isLoading && <Spinner animation="grow" />}</div>;
  }
}

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired,
};

export default Loader;
