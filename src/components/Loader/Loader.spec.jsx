import React from 'react';
import { mount } from 'enzyme';
import { Spinner } from 'react-bootstrap';
import Loader from './Loader';

let LoaderWrapper;

const render = (props = {}) => mount(<Loader isLoading={false} {...props} />);

describe('Loader - render', () => {
  beforeEach(() => {
    LoaderWrapper = render();
  });
  it('should return no div', () => {
    expect(LoaderWrapper.find(Spinner).exists()).toEqual(false);
  });
  it('should return div if prop is loading', () => {
    const loadingWrapper = render({
      isLoading: true,
    });
    expect(loadingWrapper.find(Spinner).exists()).toEqual(true);
  });
});
