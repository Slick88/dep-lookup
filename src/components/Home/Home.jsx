/*eslint-disable*/
import React, { Component } from 'react';
import { isEmpty, toLower } from 'lodash';
import { Button, Col, Container, Row, ListGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';
import styles from './Home.module.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: '',
    };
  }

  handleInputChange = (value) => {
    const { getSuggestedValue } = this.props;
    const processedValue = toLower(value);
    this.setState({
      searchValue: processedValue,
    });
    getSuggestedValue(processedValue);
  };

  handleSubmit = (event) => {
    if (event.key === 'Enter') {
      this.getPackages();
    }
  };

  handleSelect = (value) => {
    const processedValue = toLower(value);

    this.setState(
      {
        searchValue: processedValue,
      },
      () => this.getPackages(),
    );
  };

  getPackages = () => {
    const { history, resetSuggestedValue } = this.props;
    const { searchValue } = this.state;
    resetSuggestedValue();
    history.push(`/details/${searchValue}`);
  };

  renderSuggestion = () => {
    const { suggestedDependencies } = this.props;
    console.table(suggestedDependencies);

    if (isEmpty(suggestedDependencies)) {
      return;
    }
    return (
      <ListGroup>
        {suggestedDependencies.map((dep) => {
          return (
            <ListGroup.Item action onClick={() => this.handleSelect(dep.key)}>
              {dep.key}
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    );
  };

  render() {
    const placeholder = 'Enter a value';
    const { searchValue } = this.state;
    return (
      <Container className={styles.pageOverview}>
        <Row>
          <h1 className={styles.title}>Package Dependencies Check</h1>
        </Row>
        <Row>
          <Col lg="8" md="6" xs={6}>
            <input
              className={styles.specialInput}
              type="text"
              placeholder={placeholder}
              value={searchValue}
              onChange={(e) => this.handleInputChange(e.target.value)}
              onKeyPress={this.handleSubmit}
            />
            {this.renderSuggestion()}
          </Col>
          <Col lg="2" md="2" xs={6}>
            <Button variant="outline-primary" onClick={this.getPackages}>
              Get Dependencies
            </Button>
          </Col>
        </Row>
      </Container>
    );
  }
}

Home.propTypes = {
  getSuggestedValue: PropTypes.func.isRequired,
  resetSuggestedValue: PropTypes.func.isRequired,
  suggestedDependencies: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default Home;
