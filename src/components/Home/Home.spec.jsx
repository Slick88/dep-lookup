import React from 'react';
import { mount } from 'enzyme';
import Home from './Home';

let HomeWrapper;
let fetchDataMockFn;
let historyPushMockFn;
let getSuggestedValueMockFn;
let resetSuggestedValueMockFn;

const render = (props = {}) =>
  mount(
    <Home
      fetchData={fetchDataMockFn}
      getSuggestedValue={getSuggestedValueMockFn}
      resetSuggestedValue={resetSuggestedValueMockFn}
      suggestedDependencies={[]}
      {...props}
    />,
  );

describe('Home - render', () => {
  beforeEach(() => {
    fetchDataMockFn = jest.fn();
    getSuggestedValueMockFn = jest.fn();
    resetSuggestedValueMockFn = jest.fn();
    HomeWrapper = render({});
  });
  it('should render all components correctly', () => {
    expect(HomeWrapper).toMatchSnapshot();
    expect(HomeWrapper.find('input').exists()).toEqual(true);
    expect(HomeWrapper.find('button').text()).toEqual('Get Dependencies');
  });
  it('should render the List if there is suggestedDependencies', () => {
    expect(HomeWrapper.find('.list-group').exists()).toEqual(false);
    HomeWrapper.setProps({
      suggestedDependencies: [{ key: 'test', value: 'test' }],
    });
    expect(HomeWrapper.find('.list-group').exists()).toEqual(true);
  });
});

describe('Home - functions', () => {
  beforeEach(() => {
    fetchDataMockFn = jest.fn();
    historyPushMockFn = jest.fn();
    getSuggestedValueMockFn = jest.fn();
    resetSuggestedValueMockFn = jest.fn();
    HomeWrapper = render({
      history: {
        push: historyPushMockFn,
      },
    });
  });
  it('handleInputChange should set state correctly', () => {
    HomeWrapper.instance().handleInputChange('test');
    expect(HomeWrapper.state('searchValue')).toEqual('test');
  });
  it('handleSelect should should set state correctly', () => {
    HomeWrapper.instance().handleSelect('test');
    expect(HomeWrapper.state('searchValue')).toEqual('test');
    expect(fetchDataMockFn).toBeCalledTimes(1);
    expect(historyPushMockFn).toBeCalledTimes(1);
  });
  it('getPackages should call correct functions', () => {
    expect(fetchDataMockFn).toBeCalledTimes(0);
    expect(historyPushMockFn).toBeCalledTimes(0);
    HomeWrapper.instance().getPackages();
    expect(fetchDataMockFn).toBeCalledTimes(1);
    expect(historyPushMockFn).toBeCalledTimes(1);
  });
});
