// import React, {Component} from 'react';
import { connect } from 'react-redux';
import Loader from '../components/Loader';

const mapStateToProps = (state) => ({
  isLoading: state.Loader.isLoading || '',
});
const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Loader);
