// import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Details from '../components/Details';

// Add ducks for containers
import detailsDucks from '../ducks/Details';

const { fetchData } = detailsDucks.operations;

const mapStateToProps = (state) => ({
  packageName: state.Details.packageName || '',
  list: state.Details.list || [],
  isLoading: state.Loader.isLoading || false,
});
const mapDispatchToProps = {
  fetchData,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Details),
);
