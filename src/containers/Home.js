import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Home from '../components/Home';

// Add ducks for containers
import detailsDucks from '../ducks/Details';
import homeDucks from '../ducks/Home';

const { fetchData } = detailsDucks.operations;
const { getSuggestedValue, resetSuggestedValue } = homeDucks.operations;

const mapStateToProps = (state) => ({
  suggestedDependencies: state.Home.suggestedValue || [],
});
const mapDispatchToProps = {
  fetchData,
  getSuggestedValue,
  resetSuggestedValue,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Home),
);
