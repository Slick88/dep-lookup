import actions from './actions';
import types from './types';

describe('actions - trigger storeApiData', () => {
  it('should trigger storeApiData', () => {
    const packageName = 'title';
    const list = ['abc', 'def'];
    const expectedAction = {
      type: types.LOAD_PACKAGE_API,
      packageName,
      list,
    };
    expect(actions.storeApiData(packageName, list)).toEqual(expectedAction);
  });
});
