import axios from 'axios';
import { isUndefined, isEmpty, capitalize } from 'lodash';
import actions from './actions';
import loaderActions from '../Loader/actions';

const getDependanciesClosure = () => {
  const storeValue = [];
  const getOnlineValue = async (value) => {
    // eslint-disable-next-line no-console
    console.warn('getting for ', value);
    if (!storeValue.includes(value)) {
      storeValue.push(value);
    }
    const urlX = `https://npm-registry-proxy.glitch.me/${value}/latest`;
    const response = await axios.get(urlX).catch((error) => {
      // handle error
      // eslint-disable-next-line no-console
      console.log(error);
    });
    if (!isUndefined(response)) {
      const packageData = response.data;
      if (!isUndefined(packageData.dependencies)) {
        const depArray = Object.keys(packageData.dependencies);
        return { storeValue, depArray };
      }
    }
    return { storeValue, depArray: [] };
  };
  return getOnlineValue;
};

const fetchData = (value) => async (dispatch) => {
  dispatch(loaderActions.activateLoader());
  const getOnlineValue = getDependanciesClosure();
  const result = await getOnlineValue(value);
  while (!isEmpty(result.depArray)) {
    const iterationDep = [...result.depArray];
    result.depArray = [];

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < iterationDep.length; i++) {
      // eslint-disable-next-line no-await-in-loop
      const abc = await getOnlineValue(iterationDep[i]);
      result.storeValue = abc.storeValue;
      const updateDepArray = abc.depArray.filter(
        (dep) => !result.depArray.includes(dep),
      );
      result.depArray = [...result.depArray, ...updateDepArray];
    }
  }
  const packageName = capitalize(result.storeValue.shift());
  dispatch(actions.storeApiData(packageName, result.storeValue));
  dispatch(loaderActions.deactivateLoader());
};

export default {
  fetchData,
};
