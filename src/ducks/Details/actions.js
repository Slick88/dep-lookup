import types from './types';

const storeApiData = (packageName, list) => ({
  type: types.LOAD_PACKAGE_API,
  packageName,
  list,
});

export default {
  storeApiData,
};
