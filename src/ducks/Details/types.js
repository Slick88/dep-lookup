const INITIATE_DETAILS = 'INITIATE_DETAILS';
const LOAD_PACKAGE_API = 'LOAD_PACKAGE_API';

export default {
  INITIATE_DETAILS,
  LOAD_PACKAGE_API,
};
