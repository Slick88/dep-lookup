import types from './types';

const initialState = {
  packageName: '',
  list: [],
};

const movie = (state = initialState, action) => {
  switch (action.type) {
    case types.INITIATE_DETAILS: {
      return initialState;
    }
    case types.LOAD_PACKAGE_API: {
      return { packageName: action.packageName, list: action.list };
    }
    default:
      return state;
  }
};

export default movie;
