import actions from './actions';
import types from './types';

describe('actions - trigger activateLoader', () => {
  it('should trigger activateLoader', () => {
    const expectedAction = {
      type: types.ACTIVATE_LOADER,
    };
    expect(actions.activateLoader()).toEqual(expectedAction);
  });
});

describe('actions - trigger deactivateLoader', () => {
  it('should trigger deactivateLoader', () => {
    const expectedAction = {
      type: types.DEACTIVATE_LOADER,
    };
    expect(actions.deactivateLoader()).toEqual(expectedAction);
  });
});
