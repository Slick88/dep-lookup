import types from './types';

const activateLoader = () => ({
  type: types.ACTIVATE_LOADER,
});

const deactivateLoader = () => ({
  type: types.DEACTIVATE_LOADER,
});

export default {
  activateLoader,
  deactivateLoader,
};
