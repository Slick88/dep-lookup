import types from './types';

const initialState = {
  isLoading: false,
};

const loader = (state = initialState, action) => {
  switch (action.type) {
    case types.INITIATE_DETAILS: {
      return initialState;
    }
    case types.ACTIVATE_LOADER: {
      return { isLoading: true };
    }
    case types.DEACTIVATE_LOADER: {
      return { isLoading: false };
    }
    default:
      return state;
  }
};

export default loader;
