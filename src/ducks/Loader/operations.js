/* eslint-disable*/
import actions from './actions';

const activateLoader = () => (dispatch) => {
  dispatch(actions.activateLoader());
};

const deactivateLoader = () => (dispatch) => {
  dispatch(actions.deactivateLoader());
};

export default {
  activateLoader,
  deactivateLoader,
};
