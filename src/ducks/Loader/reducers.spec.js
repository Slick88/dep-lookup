import reducer from './reducers';
import types from './types';

it('should load initial state', () => {
  const initialState = [];
  const action = {
    type: types.INITIATE_DETAILS,
  };
  const result = reducer(initialState, action);
  expect(result.isLoading).toEqual(false);
});

it('should activate loader', () => {
  const initialState = [];
  const action = {
    type: types.ACTIVATE_LOADER,
  };
  const result = reducer(initialState, action);
  expect(result.isLoading).toEqual(true);
});

it('should deactivate loader', () => {
  const initialState = [];
  const action = {
    type: types.DEACTIVATE_LOADER,
  };
  const result = reducer(initialState, action);
  expect(result.isLoading).toEqual(false);
});
