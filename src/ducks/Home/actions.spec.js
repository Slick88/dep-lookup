import actions from './actions';
import types from './types';

describe('actions - trigger storeSuggestedValue', () => {
  it('should trigger storeSuggestedValue', () => {
    const suggestedValue = [{ key: 'test', value: 'test' }];
    const expectedAction = {
      type: types.STORE_SUGGESTED_VALUE,
      suggestedValue,
    };
    expect(actions.storeSuggestedValue(suggestedValue)).toEqual(expectedAction);
  });
});

describe('actions - trigger resetSuggestedValue', () => {
  it('should trigger resetSuggestedValue', () => {
    const expectedAction = {
      type: types.RESET_SUGGESTED_VALUE,
    };
    expect(actions.resetSuggestedValue()).toEqual(expectedAction);
  });
});
