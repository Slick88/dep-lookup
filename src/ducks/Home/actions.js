import types from './types';

const storeSuggestedValue = (suggestedValue) => ({
  type: types.STORE_SUGGESTED_VALUE,
  suggestedValue,
});

const resetSuggestedValue = () => ({
  type: types.RESET_SUGGESTED_VALUE,
});

export default {
  storeSuggestedValue,
  resetSuggestedValue,
};
