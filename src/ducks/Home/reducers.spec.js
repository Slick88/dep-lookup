import reducer from './reducers';
import types from './types';

it('should load initial state', () => {
  const initialState = {
    suggestedValue: [],
  };
  const action = {
    type: types.INITIATE_DETAILS,
  };
  const result = reducer(initialState, action);
  expect(result.suggestedValue).toEqual([]);
});

it('should activate loader', () => {
  const suggestedValue = ['abc', 'def'];
  const action = {
    type: types.STORE_SUGGESTED_VALUE,
    suggestedValue,
  };
  const result = reducer(suggestedValue, action);
  expect(result.suggestedValue).toEqual(suggestedValue);
});

it('should deactivate loader', () => {
  const initialState = ['adsa'];
  const action = {
    type: types.RESET_SUGGESTED_VALUE,
  };
  const result = reducer(initialState, action);
  expect(result.suggestedValue).toEqual([]);
});
