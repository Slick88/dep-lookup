import types from './types';

const initialState = {
  suggestedValue: [],
};

const movie = (state = initialState, action) => {
  switch (action.type) {
    case types.INITIATE_HOME: {
      return initialState;
    }
    case types.STORE_SUGGESTED_VALUE: {
      return { suggestedValue: action.suggestedValue };
    }
    case types.RESET_SUGGESTED_VALUE: {
      return { suggestedValue: [] };
    }
    default:
      return state;
  }
};

export default movie;
