/* eslint-disable*/
import axios from 'axios';
import actions from './actions';

const getSuggestedValue = (value) => async (dispatch) => {
  const urlX =
    'https://npm-registry-proxy.glitch.me/search/suggestions?q=' + value;
  const suggestedValue = await axios
    .get(urlX)
    .then((response) => {
      // handle success
      return response.data.map((entry) => {
        return { key: entry.name, value: entry.name };
      });
    })
    .catch((error) => {
      // handle error
      console.log(error);
    });
  dispatch(actions.storeSuggestedValue(suggestedValue));
};

const resetSuggestedValue = () => (dispatch) => {
  dispatch(actions.resetSuggestedValue());
};

export default {
  getSuggestedValue,
  resetSuggestedValue,
};
